/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Vista;

import Modelo.Persona;
import Negocio.Dane;

/**
 *
 * @author madarme
 */
public class TestPersona {

    public static void main(String[] args) {
        //Crear un arreglo de 4 personas
        Dane dane = new Dane(3);
        //Persona(long cedula, String nombre, boolean sexo, short edad)
        Persona uno = new Persona(101259, "Edgar Parada", false, (short) 20);
        Persona dos = new Persona(125982, "Josue Gelvez", false, (short) 16);
        Persona tres = new Persona(25982, "Jehnsy Mendoza", false, (short) 31);
        //Insertar a la colección(Conjunto) de Dane:
        dane.insertarPersona(0, tres);
        dane.insertarPersona(1, dos);
        dane.insertarPersona(2, uno);
        //dane.insertarPersona(4, tres);

        //se invoca automáticamente el toString()
        System.out.println("la persona que está en pos 1 es:" + dane.consultarPersona(1));
        System.out.println("la persona que está en pos 2 es:" + dane.consultarPersona(2));
        System.out.println("la persona que está en pos 0 es:" + dane.consultarPersona(0));
        System.out.println("El tamaño de el arreglo que contiene dane es:" + dane.length());

        System.out.println(dane);

        if (dos.equals(tres)) {
            System.out.println("Son iguales");
        } else {
            System.out.println("No son iguales");
        }

        int c = dos.compareTo(tres);

        switch (c) {
            case 0:
                System.out.println("SOn iguales ");
                break;
            case 1:
                System.out.println("Objeto es mayor a objeto2");
                break;
            default:
                System.out.println("Objeto es mayor a objeto2");
                break;
        }
        
        dane.burbuja();
        System.out.println(dane);

    }

}
