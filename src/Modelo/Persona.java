/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Modelo;

/**
 *
 * @author madarme
 */
public class Persona implements Comparable {

    private long cedula;
    private String nombre;
    private boolean sexo;
    private short edad;

    public Persona() {
    }

    public Persona(long cedula, String nombre, boolean sexo, short edad) {
        this.cedula = cedula;
        this.nombre = nombre;
        this.sexo = sexo;
        this.edad = edad;
    }

    public long getCedula() {
        return cedula;
    }

    public void setCedula(long cedula) {
        this.cedula = cedula;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public boolean isSexo() {
        return sexo;
    }

    public void setSexo(boolean sexo) {
        this.sexo = sexo;
    }

    public short getEdad() {
        return edad;
    }

    public void setEdad(short edad) {
        this.edad = edad;
    }

    @Override
    public String toString() {
        return "Persona{" + "cedula=" + cedula + ", nombre=" + nombre + ", sexo=" + sexo + ", edad=" + edad + '}';
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 89 * hash + (int) (this.cedula ^ (this.cedula >>> 32));
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Persona other = (Persona) obj;
        if (this.cedula != other.cedula) {
            return false;
        }
        return true;
    }

    /**
     * comparetO = ob1-obj2
     *
     * Si obj1 == obj2 -> retorna 0 Si obj1 > obj2 -> retorna >0 Si obj1
     * < obj2 -> retorna <0
     *
     * @param o
     * @return
     */
    @Override
    public int compareTo(Object obj) {
        if (this == obj) {
            return 0;
        }
        if (obj == null) {
            return -1;
        }
        if (getClass() != obj.getClass()) {
            return -1;
        }
        final Persona other = (Persona) obj;
        //long cedula = this.cedula - other.cedula;
        //long cedula = this.edad - other.edad;
        long cedula = this.nombre.compareTo(other.nombre);
        if (cedula == 0) {
            return 0;
        } else if (cedula < 0) {
            return -1;
        } else {
            return 1;
        }

    }

}
