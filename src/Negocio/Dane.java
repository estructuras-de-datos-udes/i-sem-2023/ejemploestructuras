/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Negocio;

import Modelo.Persona;

/**
 *
 * @author madarme
 */
public class Dane {

    private Persona personas[];

    public Dane() {
    }

    public Dane(int cantidadPersonas) {
        this.personas = new Persona[cantidadPersonas];
    }

    public void insertarPersona(int posicion, Persona nueva) {
        //Cuando no puedo insertar:

        this.personas[posicion] = nueva;
    }

    public Persona consultarPersona(int posicion) {
        return this.personas[posicion];
    }

    public int length() {
        return this.personas.length;
    }

    public String toString() {
        String mensaje = "";
        //Foreach
        for (Persona nueva : this.personas) {
            mensaje += nueva.toString()+"\n";
        }
        return mensaje;
    }
    
    
    
    
    public  void burbuja() {
        int i, j;
        Persona A[]=this.personas;
        Persona aux;
        for (i = 0; i < A.length - 1; i++) {
            for (j = 0; j < A.length - i - 1; j++) {  
                
                int c=A[j + 1].compareTo(A[j]);
                
                
                if (c < 0) {
                    aux = A[j + 1];
                    A[j + 1] = A[j];
                    A[j] = aux;
                }
            }
        }
}
    
    
    
}
